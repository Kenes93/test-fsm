create table fms_currency
(
   id                 bigserial             not null primary key,
    currency_code varchar(255),
    currency_name varchar(255)
);

create table fms_currency_rate
(
    id                 bigserial             not null primary key,
    rate_date date,
    rate_value numeric(19, 2) default null,
    currency_id bigint
        constraint fk_rate_currency
            references fms_currency
);