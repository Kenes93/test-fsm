package kz.dilau.htc.filemanager.service.impl;

import kz.dilau.htc.filemanager.domain.Currency;
import kz.dilau.htc.filemanager.exception.BadRequestException;
import kz.dilau.htc.filemanager.repository.CurrencyRepository;
import kz.dilau.htc.filemanager.service.CurrencyService;
import kz.dilau.htc.filemanager.util.CurrencyName;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CurrencyServiceImpl implements CurrencyService {
    private final CurrencyName currencyName;
    private final CurrencyRepository currencyRepository;

    public boolean addCurrency(String currencyCode) {
        if (currencyRepository.existsByCurrencyCode(currencyCode))
            throw BadRequestException.createTemplateException("error.currency.exist");
        String name = currencyName.getCurrency(currencyCode).getCurrencyNameResult();
        Currency currency = new Currency();
        currency.setCurrencyCode(currencyCode);
        currency.setCurrencyName(name);
        currencyRepository.save(currency);
        return true;
    }
}
