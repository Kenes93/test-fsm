package kz.dilau.htc.filemanager.service;

public interface CurrencyService {
    boolean addCurrency(String currencyCode);
}
