package kz.dilau.htc.filemanager.service.impl;

import kz.dilau.htc.filemanager.domain.Currency;
import kz.dilau.htc.filemanager.domain.CurrencyRate;
import kz.dilau.htc.filemanager.exception.NotFoundException;
import kz.dilau.htc.filemanager.repository.CurrencyRateRepository;
import kz.dilau.htc.filemanager.repository.CurrencyRepository;
import kz.dilau.htc.filemanager.service.CurrencyRateService;
import kz.dilau.htc.filemanager.web.dto.CurrencyRateDto;
import kz.dilau.htc.filemanager.web.dto.CurrencyRatesViewDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CurrencyRateServiceImpl implements CurrencyRateService {
    private final CurrencyRateRepository currencyRateRepository;
    private final CurrencyRepository currencyRepository;

    @Override
    public Long save(CurrencyRateDto dto) {
        Optional<Currency> currency = currencyRepository.findByCurrencyCode(dto.getCurrencyCode());
        if (!currency.isPresent()) {
            throw NotFoundException.currencyNotFound();
        }
        CurrencyRate rate = new CurrencyRate();
        rate.setCurrency(currency.get());
        rate.setRateDate(dto.getRateDate());
        rate.setRateValue(dto.getRateValue());
        currencyRateRepository.save(rate);
        return rate.getId();
    }

    public List<CurrencyRatesViewDto> getAllByDate(LocalDate date) {
        List<CurrencyRate> currencyRates = currencyRateRepository.findAllByRateDate(date);
        return currencyRates.stream()
                .map(this::mapToCurrencyRatesDto)
                .collect(Collectors.toList());
    }

    private CurrencyRatesViewDto mapToCurrencyRatesDto(CurrencyRate rate) {
        Currency currency = currencyRepository.findById(rate.getCurrencyId()).orElse(null);
        return CurrencyRatesViewDto.builder()
                .currencyCode(currency != null ? currency.getCurrencyCode() : null)
                .currencyName(currency != null ? currency.getCurrencyName() : null)
                .rateValue(rate.getRateValue())
                .build();
    }
}
