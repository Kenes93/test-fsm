package kz.dilau.htc.filemanager.service;

import kz.dilau.htc.filemanager.web.dto.CurrencyRateDto;
import kz.dilau.htc.filemanager.web.dto.CurrencyRatesViewDto;

import java.time.LocalDate;
import java.util.List;

public interface CurrencyRateService {
    Long save(CurrencyRateDto dto);

    List<CurrencyRatesViewDto> getAllByDate(LocalDate date);
}
