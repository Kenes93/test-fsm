package kz.dilau.htc.filemanager.web.rest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import kz.dilau.htc.filemanager.service.CurrencyRateService;
import kz.dilau.htc.filemanager.service.CurrencyService;
import kz.dilau.htc.filemanager.util.Constants;
import kz.dilau.htc.filemanager.web.dto.CurrencyRateDto;
import kz.dilau.htc.filemanager.web.dto.CurrencyRatesViewDto;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@RestController
@ApiModel(description = "Rest Controller для работы с курсом валют")
@RequestMapping(Constants.RATE_REST_ENDPOINT)
public class CurrencyRateResource {
    private final CurrencyRateService currencyRateService;
    private final CurrencyService currencyService;

    @PostMapping
    public ResponseEntity<Long> saveCurrencyRate(@ApiParam(value = "Модель для создания записи") @RequestBody CurrencyRateDto dto) {
        Long result = currencyRateService.save(dto);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{date}")
    public ResponseEntity<List<CurrencyRatesViewDto>> getCurrencyRatesByDate(@ApiParam(value = "Дата фильтрации") @PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        List<CurrencyRatesViewDto> resultList = currencyRateService.getAllByDate(date);
        return ResponseEntity.ok(resultList);
    }

    @PostMapping("/addCurrencyName")
    public ResponseEntity<Boolean> addCurrencyName(@ApiParam(value = "Код валюты") @RequestParam String currencyCode) {
        boolean result = currencyService.addCurrency(currencyCode);
        return ResponseEntity.ok(result);
    }

}
