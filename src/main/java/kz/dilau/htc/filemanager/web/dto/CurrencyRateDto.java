package kz.dilau.htc.filemanager.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "CurrencyRateDto", description = "Модель добавления информации о курсе валют")
public class CurrencyRateDto {
    @ApiModelProperty(value = "Код валюты")
    private String currencyCode;
    @ApiModelProperty(value = "Курс валюты")
    private BigDecimal rateValue;
    @ApiModelProperty(value = "Дата добавления информации")
    private LocalDate rateDate;
}
