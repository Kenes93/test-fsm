package kz.dilau.htc.filemanager.web.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CurrencyRatesViewDto {
    @ApiModelProperty(value = "Код валюты")
    private String currencyCode;
    @ApiModelProperty(value = "Полное наименование валюты валюты")
    private String currencyName;
    @ApiModelProperty(value = "Курс валюты")
    private BigDecimal rateValue;
}
