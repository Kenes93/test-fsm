package kz.dilau.htc.filemanager.web.dto.common;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Модель пагинации")
public class PageableDto {
    @PositiveOrZero
    @ApiModelProperty(notes = "Начальная страница", required = true)
    protected int pageNumber = 0;

    @Positive
    @ApiModelProperty(notes = "Количество страниц", required = true, example = "10")
    protected int pageSize = 10;

    @ApiModelProperty(notes = "Сортировка по полю", required = false, example = "id")
    protected String sortBy = "id";

    @ApiModelProperty(notes = "Направление сортировки", required = false)
    protected Sort.Direction direction = Sort.Direction.DESC;
}

