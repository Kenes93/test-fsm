package kz.dilau.htc.filemanager.exception;

import kz.dilau.htc.filemanager.util.BundleMessageUtil;
import kz.dilau.htc.filemanager.web.dto.common.LocaledValue;
import org.springframework.http.HttpStatus;

public class BadRequestException extends DetailedException {
    public BadRequestException(LocaledValue description) {
        super(HttpStatus.BAD_REQUEST, description);
    }

    public static BadRequestException createTemplateException(String name) {
        return new BadRequestException(BundleMessageUtil.getLocaledValue(name));
    }
}
