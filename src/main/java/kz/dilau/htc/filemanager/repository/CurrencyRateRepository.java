package kz.dilau.htc.filemanager.repository;

import kz.dilau.htc.filemanager.domain.CurrencyRate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface CurrencyRateRepository extends JpaRepository<CurrencyRate, Long> {
    List<CurrencyRate> findAllByRateDate(LocalDate date);
}
