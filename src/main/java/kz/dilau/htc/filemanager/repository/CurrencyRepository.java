package kz.dilau.htc.filemanager.repository;

import kz.dilau.htc.filemanager.domain.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CurrencyRepository extends JpaRepository<Currency, Long> {
    Optional<Currency> findByCurrencyCode(String code);

    boolean existsByCurrencyCode(String code);
}
