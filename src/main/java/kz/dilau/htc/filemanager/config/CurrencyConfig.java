package kz.dilau.htc.filemanager.config;

import kz.dilau.htc.filemanager.util.CurrencyName;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class CurrencyConfig {
    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("kz.fms.springsoap.client.gen");
        return marshaller;
    }

    @Bean
    public CurrencyName countryClient(Jaxb2Marshaller marshaller) {
        CurrencyName client = new CurrencyName();
        client.setDefaultUri("http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

}
