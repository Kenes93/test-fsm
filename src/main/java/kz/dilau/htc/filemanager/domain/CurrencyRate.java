package kz.dilau.htc.filemanager.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kz.dilau.htc.filemanager.domain.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

import static kz.dilau.htc.filemanager.util.Constants.TABLE_NAME_PREFIX;

@Entity
@Getter
@Setter
@Table(name = TABLE_NAME_PREFIX + "currency_rate")
public class CurrencyRate extends BaseEntity<Long> {
    @ManyToOne
    @JoinColumn(name = "currency_id")
    private Currency currency;
    @Column(name = "currency_id", insertable = false, updatable = false)
    private Long currencyId;
    @Column(name = "rate_value")
    private BigDecimal rateValue;
    @Column(name = "rate_date")
    @JsonIgnore
    private LocalDate rateDate;

    public CurrencyRate() {
    }
}
