package kz.dilau.htc.filemanager.domain;

import kz.dilau.htc.filemanager.domain.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import static kz.dilau.htc.filemanager.util.Constants.TABLE_NAME_PREFIX;

@Getter
@Setter
@Entity
@Table(name = TABLE_NAME_PREFIX + "currency")
public class Currency extends BaseEntity<Long> {
    @Column(name = "currency_code")
    private String currencyCode;
    @Column(name = "currency_name")
    private String currencyName;
}
