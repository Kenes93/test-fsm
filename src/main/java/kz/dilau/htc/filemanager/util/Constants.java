package kz.dilau.htc.filemanager.util;

public class Constants {
    public static final String API_PATH = "/api";
    public static final String OPEN_API_PATH = "/open-api";
    public static final String RATE_REST_ENDPOINT = "/rate";
    public static final String TABLE_NAME_PREFIX = "fms_";
    public static final String AUTHORIZATION_PREFIX = "Bearer";
    private Constants() {}
}
