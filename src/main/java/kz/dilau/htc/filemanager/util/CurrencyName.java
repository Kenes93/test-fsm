package kz.dilau.htc.filemanager.util;

import kz.fms.springsoap.client.gen.CurrencyNameResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class CurrencyName extends WebServiceGatewaySupport {

    public CurrencyNameResponse getCurrency(String currency) {
        kz.fms.springsoap.client.gen.CurrencyName currencyName = new kz.fms.springsoap.client.gen.CurrencyName();
        currencyName.setSCurrencyISOCode(currency);
        return (CurrencyNameResponse) getWebServiceTemplate()
                .marshalSendAndReceive(currencyName);
    }
}
